﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Runtime.InteropServices;

namespace OfficeTranslator
{
    public partial class MultitranControl : UserControl
    {
        public MultitranControl()
        {
            InitializeComponent();
            DisableClickSounds();
        }

        string lastTerm;

        #region PInvoke
        const int FEATURE_DISABLE_NAVIGATION_SOUNDS = 21;
        const int SET_FEATURE_ON_PROCESS = 0x00000002;

        [DllImport("urlmon.dll")]
        [PreserveSig]
        [return: MarshalAs(UnmanagedType.Error)]
        static extern int CoInternetSetFeatureEnabled(
            int FeatureEntry,
            [MarshalAs(UnmanagedType.U4)] int dwFlags,
            bool fEnable);

        static void DisableClickSounds()
        {
            CoInternetSetFeatureEnabled(
                FEATURE_DISABLE_NAVIGATION_SOUNDS,
                SET_FEATURE_ON_PROCESS,
                true);
        }
        #endregion

        public void DisplayTerm(string term)
        {
            term = term.Trim();            
            webBrowser.ScriptErrorsSuppressed = true;            
            webBrowser.Navigate(string.Format("http://www.multitran.ru/c/m.exe?s={0}&&l1=1", term));                                    
        }

        private void webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            var elem = webBrowser.Document.GetElementById("translation");            
            if (elem != null)
            {
                var parent = elem.Parent;
                if (parent != null)
                {
                    var input = elem.GetElementsByTagName("input");
                    if (input.Count >= 3)
                    {
                        if (!string.IsNullOrWhiteSpace(toolStripTextBoxSearch.Text))
                            lastTerm = toolStripTextBoxSearch.Text;                                                    
                        toolStripTextBoxSearch.Text = input[2].GetAttribute("value");
                    }
                    var firstChild = parent.FirstChild;
                    if (firstChild != null)
                        firstChild.OuterHtml = "";
                    elem.OuterHtml = "";                    
                    webBrowser.DocumentText = "<body>" + parent.InnerHtml + "</body>";
                }
            }
        }

        private void webBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            var url = e.Url.ToString();
            const string redirectStart = "about:m.exe";
            if (url.StartsWith(redirectStart))                           
            {
                e.Cancel = true;
                url = url.Replace("about:", "http://www.multitran.ru/c/");
                webBrowser.Navigate(url);                
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            DisplayTerm(toolStripTextBoxSearch.Text);
        }

        private void toolStripTextBoxSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                DisplayTerm(toolStripTextBoxSearch.Text);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(lastTerm))
                DisplayTerm(lastTerm);
        }
    }
}
