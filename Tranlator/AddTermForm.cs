﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OfficeTranslator
{
    public partial class AddTermForm : Form
    {
        public AddTermForm()
        {
            InitializeComponent();
        }

        public Tuple<string, string, string> Exec(string term, string target)
        {
            textBoxTerm.Text = term;            
            textBoxDesc.Text = "";
            textBoxDest.Text = target;
            ActiveControl = textBoxDest;
            if (ShowDialog() == DialogResult.OK)
            {
                return new Tuple<string, string, string>(textBoxTerm.Text, textBoxDest.Text, textBoxDesc.Text);
            }
            else
            {
                return null;
            }
        }

        public bool Exec(TermVariant term)
        {
            textBoxTerm.Text = term.Source;
            textBoxDesc.Text = term.Description;
            textBoxDest.Text = term.Value;
            ActiveControl = textBoxDest;
            if (ShowDialog() == DialogResult.OK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void textBoxDest_TextChanged(object sender, EventArgs e)
        {
            buttonOK.Enabled = (textBoxDest.Text.Length > 0) && (textBoxTerm.Text.Length > 0);
        }
    }
}
