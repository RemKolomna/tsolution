﻿namespace OfficeTranslator
{
    partial class TranslateControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TranslateControl));
            this.panel1 = new System.Windows.Forms.Panel();
            this.mainBar = new System.Windows.Forms.ToolStrip();
            this.AddTermButton = new System.Windows.Forms.ToolStripSplitButton();
            this.addTermModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lastTranslationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyFromSourceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transliterateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMultitran = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnPreConfirm = new System.Windows.Forms.ToolStripButton();
            this.btnConfirm = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageTM = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ColumnSource = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTarget = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageTerms = new System.Windows.Forms.TabPage();
            this.treeViewTerms = new System.Windows.Forms.TreeView();
            this.contextMenuTerms = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editTermToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuSource = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addTermToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textTranslation = new RichTextBoxEx();
            this.textSource = new RichTextBoxEx();
            this.panel1.SuspendLayout();
            this.mainBar.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPageTM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPageTerms.SuspendLayout();
            this.contextMenuTerms.SuspendLayout();
            this.contextMenuSource.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textTranslation);
            this.panel1.Controls.Add(this.mainBar);
            this.panel1.Controls.Add(this.splitter2);
            this.panel1.Controls.Add(this.textSource);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(462, 413);
            this.panel1.TabIndex = 3;
            // 
            // mainBar
            // 
            this.mainBar.AutoSize = false;
            this.mainBar.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.mainBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddTermButton,
            this.btnMultitran,
            this.toolStripSeparator1,
            this.btnPreConfirm,
            this.btnConfirm,
            this.toolStripSeparator2,
            this.toolStripButton5});
            this.mainBar.Location = new System.Drawing.Point(0, 76);
            this.mainBar.Name = "mainBar";
            this.mainBar.Size = new System.Drawing.Size(462, 28);
            this.mainBar.TabIndex = 5;
            this.mainBar.Text = "toolStrip1";
            // 
            // AddTermButton
            // 
            this.AddTermButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.AddTermButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTermModeToolStripMenuItem,
            this.lastTranslationToolStripMenuItem,
            this.copyFromSourceToolStripMenuItem,
            this.transliterateToolStripMenuItem});
            this.AddTermButton.Image = ((System.Drawing.Image)(resources.GetObject("AddTermButton.Image")));
            this.AddTermButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AddTermButton.Name = "AddTermButton";
            this.AddTermButton.Size = new System.Drawing.Size(73, 25);
            this.AddTermButton.Text = "Add term";
            this.AddTermButton.ButtonClick += new System.EventHandler(this.addTermModeToolStripMenuItem_Click);
            // 
            // addTermModeToolStripMenuItem
            // 
            this.addTermModeToolStripMenuItem.Name = "addTermModeToolStripMenuItem";
            this.addTermModeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addTermModeToolStripMenuItem.Text = "Add term mode";
            this.addTermModeToolStripMenuItem.Click += new System.EventHandler(this.addTermModeToolStripMenuItem_Click);
            // 
            // lastTranslationToolStripMenuItem
            // 
            this.lastTranslationToolStripMenuItem.Name = "lastTranslationToolStripMenuItem";
            this.lastTranslationToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.lastTranslationToolStripMenuItem.Text = "Last translation";
            this.lastTranslationToolStripMenuItem.Click += new System.EventHandler(this.lastTranslationToolStripMenuItem_Click);
            // 
            // copyFromSourceToolStripMenuItem
            // 
            this.copyFromSourceToolStripMenuItem.Name = "copyFromSourceToolStripMenuItem";
            this.copyFromSourceToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.copyFromSourceToolStripMenuItem.Text = "Copy from source";
            this.copyFromSourceToolStripMenuItem.Click += new System.EventHandler(this.copyFromSourceToolStripMenuItem_Click);
            // 
            // transliterateToolStripMenuItem
            // 
            this.transliterateToolStripMenuItem.Name = "transliterateToolStripMenuItem";
            this.transliterateToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.transliterateToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.transliterateToolStripMenuItem.Text = "Transliterate";
            this.transliterateToolStripMenuItem.Click += new System.EventHandler(this.transliterateToolStripMenuItem_Click);
            // 
            // btnMultitran
            // 
            this.btnMultitran.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMultitran.Image = ((System.Drawing.Image)(resources.GetObject("btnMultitran.Image")));
            this.btnMultitran.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnMultitran.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMultitran.Name = "btnMultitran";
            this.btnMultitran.Size = new System.Drawing.Size(23, 25);
            this.btnMultitran.Text = "Multitran";
            this.btnMultitran.Click += new System.EventHandler(this.btnMultitran_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // btnPreConfirm
            // 
            this.btnPreConfirm.Image = ((System.Drawing.Image)(resources.GetObject("btnPreConfirm.Image")));
            this.btnPreConfirm.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnPreConfirm.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPreConfirm.Name = "btnPreConfirm";
            this.btnPreConfirm.Size = new System.Drawing.Size(61, 25);
            this.btnPreConfirm.Text = "Draft";
            this.btnPreConfirm.ToolTipText = "Draft translation";
            this.btnPreConfirm.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirm.Image")));
            this.btnConfirm.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnConfirm.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(79, 25);
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.ToolTipText = "Confirm translation (Ctrl + Enter)";
            this.btnConfirm.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(28, 25);
            this.toolStripButton5.Text = "Дальше";
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(0, 73);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(462, 3);
            this.splitter2.TabIndex = 4;
            this.splitter2.TabStop = false;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(462, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 413);
            this.splitter1.TabIndex = 4;
            this.splitter1.TabStop = false;
            this.splitter1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitter1_SplitterMoved);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(465, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(744, 413);
            this.panel2.TabIndex = 5;
            // 
            // tabControl
            // 
            this.tabControl.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl.Controls.Add(this.tabPageTM);
            this.tabControl.Controls.Add(this.tabPageTerms);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(744, 413);
            this.tabControl.TabIndex = 6;
            // 
            // tabPageTM
            // 
            this.tabPageTM.Controls.Add(this.dataGridView1);
            this.tabPageTM.Location = new System.Drawing.Point(4, 4);
            this.tabPageTM.Name = "tabPageTM";
            this.tabPageTM.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTM.Size = new System.Drawing.Size(736, 387);
            this.tabPageTM.TabIndex = 0;
            this.tabPageTM.Text = "TM";
            this.tabPageTM.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnSource,
            this.ColumnTarget});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(730, 381);
            this.dataGridView1.TabIndex = 6;
            this.dataGridView1.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentDoubleClick);
            // 
            // ColumnSource
            // 
            this.ColumnSource.DataPropertyName = "Source";
            this.ColumnSource.HeaderText = "Text";
            this.ColumnSource.Name = "ColumnSource";
            this.ColumnSource.ReadOnly = true;
            // 
            // ColumnTarget
            // 
            this.ColumnTarget.DataPropertyName = "Destination";
            this.ColumnTarget.HeaderText = "Translated";
            this.ColumnTarget.Name = "ColumnTarget";
            this.ColumnTarget.ReadOnly = true;
            // 
            // tabPageTerms
            // 
            this.tabPageTerms.Controls.Add(this.treeViewTerms);
            this.tabPageTerms.Location = new System.Drawing.Point(4, 4);
            this.tabPageTerms.Name = "tabPageTerms";
            this.tabPageTerms.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTerms.Size = new System.Drawing.Size(736, 387);
            this.tabPageTerms.TabIndex = 1;
            this.tabPageTerms.Text = "Terms";
            this.tabPageTerms.UseVisualStyleBackColor = true;
            // 
            // treeViewTerms
            // 
            this.treeViewTerms.ContextMenuStrip = this.contextMenuTerms;
            this.treeViewTerms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewTerms.Location = new System.Drawing.Point(3, 3);
            this.treeViewTerms.Name = "treeViewTerms";
            this.treeViewTerms.Size = new System.Drawing.Size(730, 381);
            this.treeViewTerms.TabIndex = 0;
            this.treeViewTerms.DoubleClick += new System.EventHandler(this.treeViewTerms_DoubleClick);
            // 
            // contextMenuTerms
            // 
            this.contextMenuTerms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editTermToolStripMenuItem});
            this.contextMenuTerms.Name = "contextMenuStrip2";
            this.contextMenuTerms.Size = new System.Drawing.Size(123, 26);
            // 
            // editTermToolStripMenuItem
            // 
            this.editTermToolStripMenuItem.Name = "editTermToolStripMenuItem";
            this.editTermToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.editTermToolStripMenuItem.Text = "Edit term";
            this.editTermToolStripMenuItem.Click += new System.EventHandler(this.editTermToolStripMenuItem_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "blue-ok-icon.png");
            this.imageList1.Images.SetKeyName(1, "Double-J-Design-Origami-Colored-Pencil-Green-ok.ico");
            // 
            // contextMenuSource
            // 
            this.contextMenuSource.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTermToolStripMenuItem});
            this.contextMenuSource.Name = "contextMenuStrip1";
            this.contextMenuSource.Size = new System.Drawing.Size(125, 26);
            // 
            // addTermToolStripMenuItem
            // 
            this.addTermToolStripMenuItem.Name = "addTermToolStripMenuItem";
            this.addTermToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.addTermToolStripMenuItem.Text = "Add term";
            this.addTermToolStripMenuItem.Click += new System.EventHandler(this.addTermToolStripMenuItem_Click);
            // 
            // textTranslation
            // 
            this.textTranslation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textTranslation.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textTranslation.HideSelection = false;
            this.textTranslation.Location = new System.Drawing.Point(0, 104);
            this.textTranslation.Name = "textTranslation";
            this.textTranslation.Size = new System.Drawing.Size(462, 309);
            this.textTranslation.TabIndex = 3;
            this.textTranslation.Text = "";
            this.textTranslation.TextChanged += new System.EventHandler(this.textTranslation_TextChanged);
            this.textTranslation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textTranslation_KeyDown);
            this.textTranslation.KeyUp += new System.Windows.Forms.KeyEventHandler(this.richTextBox2_KeyUp);
            this.textTranslation.MouseEnter += new System.EventHandler(this.textTranslation_MouseEnter);
            this.textTranslation.MouseLeave += new System.EventHandler(this.textTranslation_MouseLeave);
            // 
            // textSource
            // 
            this.textSource.AutoWordSelection = true;
            this.textSource.Dock = System.Windows.Forms.DockStyle.Top;
            this.textSource.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textSource.HideSelection = false;
            this.textSource.Location = new System.Drawing.Point(0, 0);
            this.textSource.Name = "textSource";
            this.textSource.ReadOnly = true;
            this.textSource.Size = new System.Drawing.Size(462, 73);
            this.textSource.TabIndex = 2;
            this.textSource.Text = "";
            this.textSource.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.textSource_LinkClicked);
            this.textSource.DoubleClick += new System.EventHandler(this.textSource_DoubleClick);
            this.textSource.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textSource_KeyDown);
            // 
            // TranslateControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel2);
            this.Name = "TranslateControl";
            this.Size = new System.Drawing.Size(1209, 413);
            this.panel1.ResumeLayout(false);
            this.mainBar.ResumeLayout(false);
            this.mainBar.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabPageTM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPageTerms.ResumeLayout(false);
            this.contextMenuTerms.ResumeLayout(false);
            this.contextMenuSource.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private RichTextBoxEx textTranslation;
        private RichTextBoxEx textSource;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip mainBar;
        private System.Windows.Forms.ToolStripButton btnConfirm;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolStripButton btnPreConfirm;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageTM;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTarget;
        private System.Windows.Forms.TabPage tabPageTerms;
        private System.Windows.Forms.TreeView treeViewTerms;
        private System.Windows.Forms.ContextMenuStrip contextMenuSource;
        private System.Windows.Forms.ToolStripMenuItem addTermToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuTerms;
        private System.Windows.Forms.ToolStripMenuItem editTermToolStripMenuItem;
        private System.Windows.Forms.ToolStripSplitButton AddTermButton;
        private System.Windows.Forms.ToolStripMenuItem addTermModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lastTranslationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyFromSourceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transliterateToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnMultitran;

    }
}
