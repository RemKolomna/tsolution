﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Web;

namespace OfficeTranslator
{
    enum AddTermMode
    {
        None,
        WaitSource,
        WaitTarget
    }
    public partial class TranslateControl : UserControl
    {
        TranslationMemoryDatabase translationMemory;
        SortedDictionary<int, string> activeTerms = new SortedDictionary<int, string>();
        TermsCollection termsCollection;
        string lastTranslatedText;
        AddTermMode termMode;

        void rememberTranslatedText()
        {
            var text = textTranslation.Text;
            if (!String.IsNullOrWhiteSpace(text))
                lastTranslatedText = text;
        }

        public bool MultitranOnDblClick;

        void restoreTranslatedText()
        {
            var text = lastTranslatedText;
            if (!String.IsNullOrWhiteSpace(text))
            {
                rememberTranslatedText();
                textTranslation.Text = text;
            }
        }

        public void AddContextMenu(RichTextBox rtb)
        {
            if (rtb.ContextMenuStrip == null)
            {
                ContextMenuStrip cms = new ContextMenuStrip { ShowImageMargin = false };
                ToolStripMenuItem tsmiCut = new ToolStripMenuItem("Cut");
                tsmiCut.Click += (sender, e) => rtb.Cut();
                cms.Items.Add(tsmiCut);
                ToolStripMenuItem tsmiCopy = new ToolStripMenuItem("Copy");
                tsmiCopy.Click += (sender, e) => rtb.Copy();
                cms.Items.Add(tsmiCopy);
                ToolStripMenuItem tsmiPaste = new ToolStripMenuItem("Paste");
                tsmiPaste.Click += (sender, e) => rtb.Paste();
                cms.Items.Add(tsmiPaste);

                cms.Items.Add(new ToolStripSeparator());

                if (!rtb.ReadOnly)
                {
                    ToolStripMenuItem tsmiUndo = new ToolStripMenuItem("Undo");
                    tsmiUndo.Click += (sender, e) => rtb.Undo();
                    cms.Items.Add(tsmiUndo);
                    ToolStripMenuItem tsmiRedo = new ToolStripMenuItem("Redo");
                    tsmiRedo.Click += (sender, e) => rtb.Redo();
                    cms.Items.Add(tsmiRedo);
                    cms.Items.Add(new ToolStripSeparator());
                }

                ToolStripMenuItem tsmiTerm = new ToolStripMenuItem("Add term...");
                tsmiTerm.Click += (sender, e) => addTerm(rtb.SelectedText, "");
                cms.Items.Add(tsmiTerm);

                ToolStripMenuItem tsmiMultitran = new ToolStripMenuItem("Search in multitran");
                tsmiMultitran.Click += (sender, e) => OnMultitranSearch.Invoke(rtb.SelectedText);
                cms.Items.Add(tsmiMultitran);
                

                rtb.ContextMenuStrip = cms;
            }
        }
        public TranslateControl(TranslationMemoryDatabase tm)
        {            
            translationMemory = tm;
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
            AddContextMenu(textSource);
            AddContextMenu(textTranslation);
        }

        public void SetTextForTranslation(string text)
        {
            setSourceText(text);
            textTranslation.Text = "";
        }

        void setSourceText(string source) 
        {
            textSource.Text = source;
            rememberTranslatedText();            

            string[] sentences = Regex.Split(source, @"(?<=[\.!\?])\s+");
            var translatedText = "";
            var smthtranslated = false;
            var alltranslated = true;
            foreach (var sentence in sentences)
            {

                char[] endParts = {' ', '!', '?', ':', '\n', '\t', '\r', '.', ','};
                var trims = sentence.TrimEnd(endParts);
                trims = trims.TrimStart(endParts);
                if (trims.Length < 2)
                {
                    translatedText = translatedText + sentence;
                    continue;
                }

                var sentenceending = sentence.Remove(0, trims.Length);
                var sentenceTranslation = translationMemory.exactSearchFST(sentence);
                if (sentenceTranslation.Length > 0)
                {
                    sentenceTranslation = sentenceTranslation.TrimEnd(endParts) + sentenceending;
                    if (translatedText != "")
                        translatedText += ' ';
                    translatedText = translatedText + sentenceTranslation;
                    smthtranslated = true;
                }
                else
                {
                    translatedText = translatedText + sentence;
                    alltranslated = false;
                }
            }
            if (smthtranslated)
                textTranslation.Text = translatedText;
            btnConfirm.Enabled = alltranslated;

            // do not search TM and terms on huge texts to avoid hanging
            var searchSource = source.Length < 8192 ? source : "";
            var tmMatches = translationMemory.searchFST(searchSource);
            if (tmMatches.Count > 100)
                tmMatches.RemoveRange(100, tmMatches.Count - 100);
            tabPageTM.Text = String.Format("TM ({0})", tmMatches.Count);
            dataGridView1.DataSource = tmMatches;
            updateTerms(searchSource);            
        }

        void updateSourceText()
        {
            setSourceText(textSource.Text);
        }

        void insertDestinationText(string Text)
        {
            if (textTranslation.SelectionStart == 0 && Text.Length > 0)
                Text = Char.ToUpper(Text[0]) + Text.Substring(1);

            textTranslation.SelectedText = Text;
            textTranslation.Focus();
        }

        private void richTextBox2_KeyUp(object sender, KeyEventArgs e)
        {
            
        }

        void AcceptTranslation(int Mode)
        {
            // save source and dest to TM
            if (textTranslation.TextLength > 1)
            {
                var phrase = textTranslation.Text;
                char[] endParts = { ' ', '!', '?', ':', '\n', '\t', '\r', '.', ',', '-' };
                var trimPhrase = textSource.Text.TrimEnd(endParts);
                var PhraseEnding = textSource.Text.Remove(0, trimPhrase.Length);
                Globals.ThisAddIn.Application.Selection.Text = textTranslation.Text.TrimEnd(endParts) + PhraseEnding;
                if (Mode == 2)
                {
                    Globals.ThisAddIn.Application.Selection.Comments.Add(Globals.ThisAddIn.Application.Selection.Range, "Проверить!");
                }
                translationMemory.addToInternalTM(textSource.Text, textTranslation.Text);
                WordIntf.JumpToNext(Microsoft.Office.Interop.Word.WdUnits.wdSentence);
            };
        }

        private void textTranslation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.Return)
            {
                AcceptTranslation(0);
                e.Handled = true;
            }
            if (e.Control && e.KeyCode == Keys.Down)
            {
                copyFromSource();
                e.Handled = true;
            }
            if (e.Control && e.KeyCode == Keys.M)
            {
                tabControl.SelectedTab = tabPageTM;
                e.Handled = true;
            }
            if (e.Control && e.KeyCode == Keys.T)
            {
                tabControl.SelectedTab = tabPageTerms;
                e.Handled = true;
            }
            if (e.Control && e.KeyCode >= Keys.D1 && e.KeyCode <= Keys.D9 && activeTerms != null)
            {
                var index = e.KeyCode - Keys.D1;
                if (index < activeTerms.Count)
                {                    
                    insertDestinationText(activeTerms.Values.ElementAt(index));
                    e.Handled = true;                    
                }
            }
        }

        private void splitter1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            rememberTranslatedText();
             textTranslation.Text = dataGridView1[e.ColumnIndex, e.RowIndex].Value.ToString();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            AcceptTranslation(0);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            AcceptTranslation(1);
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            AcceptTranslation(2);
        }

        void copyFromSource()
        {
            if (string.IsNullOrWhiteSpace(textSource.SelectedText))
            {
                rememberTranslatedText();
                textTranslation.Text = textSource.Text;
            }
            else
                textTranslation.SelectedText = textSource.SelectedText;
            textTranslation.Focus();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            copyFromSource();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            WordIntf.JumpToNext(Microsoft.Office.Interop.Word.WdUnits.wdSentence);
        }

        private void textTranslation_TextChanged(object sender, EventArgs e)
        {
            btnConfirm.Enabled = true;
        }

        private void textSource_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.Down)
            {
                copyFromSource();
            }
        }

        private void textSource_DoubleClick(object sender, EventArgs e)
        {
            if (MultitranOnDblClick)
                OnMultitranSearch.Invoke(textSource.SelectedText);
        }

        private void addTermToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void updateTerms(string phrase)
        {
            int TermCount = 0;
            var terms = translationMemory.searchTerms(phrase);
            var normPhrase = phrase.ToLower();
            termsCollection = terms;
            activeTerms.Clear();
            treeViewTerms.BeginUpdate();
            try
            {
                treeViewTerms.Nodes.Clear();
                foreach (var term in terms)
                {
                    if (term.Translations.Count > 0)
                    {
                        var translation = term.Translations[0].Value;
                        int termPos = 0;
                        foreach(var entry in term.Entries)
                        {
                            textSource.Select(entry.Item1, entry.Item2);
                            textSource.SetSelectionLink(true);
                            termPos = entry.Item1;
                            if (!activeTerms.ContainsKey(termPos))
                                activeTerms.Add(termPos, translation);                            
                        }
                    }
                    var node = treeViewTerms.Nodes.Add(term.Source);
                    foreach (var unit in term.Translations)
                    {
                        var text = unit.Value;
                        if (!String.IsNullOrEmpty(unit.Description))
                            text += ": " + unit.Description;
                        node.Nodes.Add(text).Tag = unit;
                        TermCount++;
                    }
                    node.Expand();
                }
            }
            finally
            {
                treeViewTerms.EndUpdate();
            }            
            textSource.Select(0, 0);
            tabPageTerms.Text = String.Format("Terms ({0})", TermCount);
        }

        private void addTerm(string term, string target)
        {
            AddTermForm form = new AddTermForm();
            var data = form.Exec(term.Trim(), target);
            if (data != null)
            {
                translationMemory.addTerm(data.Item1, data.Item2, data.Item3);
                updateSourceText();
            }
        }

        private void treeViewTerms_DoubleClick(object sender, EventArgs e)
        {
            if ((treeViewTerms.SelectedNode != null) && (treeViewTerms.SelectedNode.Tag != null))
            {
                insertDestinationText((treeViewTerms.SelectedNode.Tag as TermVariant).Value);                
            }
        }

        private void textSource_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            if (termsCollection != null)
            {
                var normTerm = translationMemory.LemmatizeSourcePhrase(e.LinkText);
                var term = termsCollection.FindTerm(normTerm);
                if (term != null && term.Translations.Count > 0)
                {
                    insertDestinationText((term.Translations.First() as TermVariant).Value);
                }
            }
        }

        private void editTermToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = treeViewTerms.SelectedNode;
            if (node == null) return;
            if (node.Tag == null) return;
            var term = node.Tag as TermVariant;
            AddTermForm form = new AddTermForm();            
            if (form.Exec(term))
            {
                translationMemory.updateTerm(term);
                updateSourceText();
            }
        }

        private void lastTranslationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            restoreTranslatedText();
        }

        private void copyFromSourceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            copyFromSource();
        }

        public void StopAddingTerm()
        {
            addTermToolStripMenuItem.Checked = false;
            termMode = AddTermMode.None;
            AddTermButton.Text = "Add term";            
        }

        private void addTermModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (termMode == AddTermMode.None)
            {
                if (textSource.SelectionLength > 1 && textTranslation.SelectionLength > 0)
                {
                    addTerm(textSource.SelectedText, textTranslation.SelectedText);
                }
                else if (textSource.SelectionLength > 1)
                {
                    textTranslation.SelectionLength = 0;
                    addTermToolStripMenuItem.Checked = true;
                    termMode = AddTermMode.WaitTarget;
                    AddTermButton.Text = "Choose target!";
                }
                else
                {
                    textSource.SelectionLength = 0;
                    textTranslation.SelectionLength = 0;
                    addTermToolStripMenuItem.Checked = true;
                    termMode = AddTermMode.WaitSource;
                    AddTermButton.Text = "Choose source!";
                }
            }
            else
            {
                StopAddingTerm();
            }
        }


        private void textTranslation_MouseLeave(object sender, EventArgs e)
        {
            if (termMode == AddTermMode.WaitTarget && textTranslation.SelectionLength > 1)
            {
                StopAddingTerm();
                addTerm(textSource.SelectedText, textTranslation.SelectedText);
            }
        }
                
        protected override bool ProcessKeyPreview(ref Message m)
        {
            var key = (Keys)m.WParam;
            if (key == Keys.Escape)
            {
                StopAddingTerm();
                return true;
            }
            return false;
        }

        private void textTranslation_MouseEnter(object sender, EventArgs e)
        {
            if (termMode == AddTermMode.WaitSource && textSource.SelectionLength > 1)
            {
                termMode = AddTermMode.WaitTarget;
                AddTermButton.Text = "Choose target!";
            }
        }

        private void textSource_MouseLeave(object sender, EventArgs e)
        {

        }

        private void transliterateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var src = textSource.SelectedText;
            if (!string.IsNullOrWhiteSpace(src))
            {
                textTranslation.SelectedText = UnidecodeSharpFork.Unidecoder.Unidecode(src);
                textTranslation.Focus();
            }
        }

        public delegate void MultitranSearchHandler(string term);
        public MultitranSearchHandler OnMultitranSearch;

        private void btnMultitran_Click(object sender, EventArgs e)
        {         
            OnMultitranSearch.Invoke(textSource.SelectedText);
        }        
    }
}
