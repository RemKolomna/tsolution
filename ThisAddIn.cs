﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;

namespace OfficeTranslator
{

    class  TranslationWindow
    {
        public TranslationWindow(Word.Window window)
        {
            this.Window = window;
        }
        public Word.Window Window { get; set; }        
    }

    public partial class ThisAddIn
    {
        Dictionary<Word.Document, TSProject> projects = new Dictionary<Word.Document, TSProject>();
        
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {            
            Application.DocumentBeforeSave += DocumentSaveEventHandler;
            Application.WindowSelectionChange +=
                new Word.ApplicationEvents4_WindowSelectionChangeEventHandler(ThisDocument_SelectionChange);            
        }

        public void StartTranslation()
        {
            if (Application.ActiveWindow != null && ActiveProject == null)
            {
                TSProject proj = new TSProject(Application.ActiveWindow);
                projects.Add(Application.ActiveDocument, proj);
            }
        }

        public bool PaneVisible
        {
            get { return ActiveProject.Pane.Visible; }
            set { ActiveProject.Pane.Visible = value; }
        }

        public bool MultitranPaneVisible
        {
            get { return ActiveProject != null ? ActiveProject.MultitranPaneVisible : false; }
            set { if (ActiveProject != null) ActiveProject.MultitranPaneVisible = value; }
        }

        private void DocumentSaveEventHandler(Microsoft.Office.Interop.Word.Document Doc, ref bool SaveAsUI, ref bool Cancel)
        {
            if (ActiveProject != null)                
                ActiveProject.Flush();
        }

        public TSProject ActiveProject
        {            
            get { 
                TSProject proj;                
                if (projects.TryGetValue(Application.ActiveDocument, out proj))
                    return projects[Application.ActiveDocument];
                else
                    return null;
            }
        }

        void ThisDocument_SelectionChange(Microsoft.Office.Interop.Word.Selection Sel)
        {
            if (ActiveProject != null)
                ActiveProject.TranslControl.SetTextForTranslation(Sel.Text);            
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
