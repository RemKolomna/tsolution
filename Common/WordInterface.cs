﻿using Word = Microsoft.Office.Interop.Word;

namespace OfficeTranslator
{
class WordIntf
{
    public static void JumpToNext(Word.WdUnits units)
    {
        var app = Globals.ThisAddIn.Application;
        var doc = app.ActiveDocument;
        var window = app.ActiveWindow;
        int SentCount = app.Selection.Sentences.Count;
        Word.Range nextRange = null;
        if (app.Selection.Start < app.Selection.End - 2)
            nextRange = app.Selection.Sentences[SentCount].Next(Microsoft.Office.Interop.Word.WdUnits.wdSentence);
        else
            nextRange = app.Selection.Sentences[SentCount];
        if (nextRange != null)
            nextRange.Select();
        app.Selection.MoveStartWhile(" /n/t");
        app.Selection.MoveEndUntil(".", Microsoft.Office.Interop.Word.WdConstants.wdBackward);
        window.ScrollIntoView(app.Selection.Range);
    }
};
};