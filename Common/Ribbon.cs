﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Tools.Ribbon;
using Word = Microsoft.Office.Interop.Word;

namespace OfficeTranslator
{
    public partial class Ribbon
    {
        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {
            
        }

        private void btnJumpSentence_Click(object sender, RibbonControlEventArgs e)
        {
            WordIntf.JumpToNext(Word.WdUnits.wdSentence);            
        }
                
        private void btnJumpParagraph_Click(object sender, RibbonControlEventArgs e)
        {
            WordIntf.JumpToNext(Word.WdUnits.wdParagraph);
        }

        private void toggleButton1_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.PaneVisible = btnTranslationPane.Checked;
        }

        private void btnTM_Click(object sender, RibbonControlEventArgs e)
        {
            if (openTMDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Globals.ThisAddIn.ActiveProject.Settings.TMFiles.Add(openTMDialog.FileName);
                Globals.ThisAddIn.ActiveProject.Flush();
            }
        }

        private void btnUpdateTM_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.ActiveProject.UpdateTM();
        }

        private void checkBoxMultitran_Click(object sender, RibbonControlEventArgs e)
        {
            var value = checkBoxMultitran.Checked;
            Globals.ThisAddIn.ActiveProject.Settings.MultitranOnDblClick = value;
            Globals.ThisAddIn.ActiveProject.TranslControl.MultitranOnDblClick = value;
        }

        private void btnMultitranPane_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.MultitranPaneVisible = !Globals.ThisAddIn.MultitranPaneVisible;
        }

        private void btnStart_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                Globals.ThisAddIn.StartTranslation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}
