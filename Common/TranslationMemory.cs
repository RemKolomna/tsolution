﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Xml;
using System.Xml.Linq;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Linq;
using LemmaSharp;

namespace OfficeTranslator
{
    class WordInfo
    {
        public WordInfo(string word, string lemma, int position)
        {
            Word = word;
            Lemma = lemma;
            Position = position;
        }
        public string Word;
        public string Lemma;
        public int Position;
    }

    class WordList: List<WordInfo>
    {
        public static char[] phraseDelemiters = { ' ', '!', '?', ':', '\n', '\t', '\r', '.', ',', '(', ')', '-' };

        public WordList() { }

        public WordList(string Phrase, Lemmatizer lemmatizer)
        {
            ParseWords(Phrase, lemmatizer);
        }

        public string NormalizedPhrase()
        {
            return string.Join(" ", this.Select(word => word.Lemma));
        }
        
        public string NormalizedPart(int start, int count)
        {
            return string.Join(" ", this.Select(word => word.Lemma), start, count);
        }

        public int ParseWords(string Phrase, Lemmatizer lemmatizer)
        {
            int length = Phrase.Length;
            int count = 0;
            int start = -1, wordLength = 0;
            for(int i = 0; i <= length; i++) // cycle extra step to add the last word inside loop
            {
                if (i < length && Array.IndexOf(phraseDelemiters, Phrase[i]) < 0)
                {
                    if (wordLength > 0)
                    {
                        wordLength++;
                    }
                    else
                    {
                        start = i;
                        wordLength = 1;
                    }
                }
                else
                {
                    if (wordLength > 0)
                    {
                        var word = Phrase.Substring(start, wordLength);
                        var lemma = lemmatizer.Lemmatize(word);
                        Add(new WordInfo(word, lemma, start));
                    }
                    wordLength = 0;
                }
            }
            return count;
        }

        public static string LemmatizePhrase(string Phrase, Lemmatizer lemmatizer)
        {
            return new WordList(Phrase, lemmatizer).NormalizedPhrase();
        }
    }

    public class TermVariant
    {
        public TermVariant(string Source, string Value, string Description, long UID)
        {
            this.Source = Source;
            this.Value = Value;
            this.Description = Description;
            this.UID = UID;
        }
        public string Source;
        public string Value;
        public string Description;
        public long UID;
    }
    
    public class TermDef
    {         
        public TermDef(string Source, string SourceNorm)
        {
            this.Source = Source;
            this.SourceNorm = SourceNorm;
            Translations = new List<TermVariant>();
            Entries = new List<Tuple<int, int>>();
        }
        public string Source { get; private set; }
        public string SourceNorm { get; private set; }
        public List<TermVariant> Translations { get; private set; }        
        public bool addTranslation(string Target, string Description, long UID)
        {
            foreach (var cur in Translations)
                if (cur.Value == Target)
                    return false;
            Translations.Add(new TermVariant(Source, Target, Description, UID));
            return true;
        }
        public bool addEntry(int start, int length)
        {
            foreach (var cur in Entries)
                if (cur.Item1 == start && cur.Item2 == length)
                    return false;
            Entries.Add(new Tuple<int, int>(start, length));
            return true;
        }
        public List<Tuple<int, int>> Entries { get; private set; }
    }

    public class TermsCollection : List<TermDef> 
    {
        public TermDef FindTerm(string TermNorm)
        {
            foreach (var cur in this)
                if (string.Equals(TermNorm, cur.SourceNorm))
                    return cur;
            return null;
        }
        public TermDef AddTerm(string Term, string TermNorm)
        {
            var term = FindTerm(Term);
            if (term == null)
            {
                term = new TermDef(Term, TermNorm);
                Add(term);
            }
            return term;
        }
    };

    public class TMUnit
    {
        public TMUnit(string src, string dest)
        {
            Source = src;
            lowerSource = src.ToLower();
            Destination = dest;
        }
        //public int relevance;
        public string Source { get; set; }
        public string lowerSource { get; set; }
        public string Destination { get; set; }
    }    

    public class TranslationMemoryDatabase
    {        
        SQLiteConnection dbConnection;        
        List<string> tmFiles = new List<string>();
        bool goodFTSIndex;
        Lemmatizer sourceLemmatizer, targetLemmatizer;
        
        public TranslationMemoryDatabase(string fileName, List<string> files)
        {
            dbConnection = new SQLiteConnection(string.Format("Data Source={0};Version=3;New=False;Compress=True;", fileName));
            dbConnection.Open();
            setTMFiles(files);
            createInternalTM();
            sourceLemmatizer = new LemmatizerPrebuiltCompact(LemmaSharp.LanguagePrebuilt.English);
            targetLemmatizer = new LemmatizerPrebuiltCompact(LemmaSharp.LanguagePrebuilt.English);            
        }

        void createInternalTM()
        {
            string sql = "SELECT count(*) FROM sqlite_master WHERE type='table' AND name='translation_units'";
            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
            var scalar = command.ExecuteScalar();
            bool hasFST = (long) scalar > 0;

            if (hasFST)
                goodFTSIndex = true;
            else
            {
                sql = @"CREATE TABLE IF NOT EXISTS `translation_units` (
	                `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	                `guid`	BLOB NOT NULL,
	                `translation_memory_id`	INT NOT NULL,
	                `source_hash`	INTEGER NOT NULL,
	                `source_segment`	TEXT,
	                `target_hash`	INTEGER NOT NULL,
	                `target_segment`	TEXT,
	                `creation_date`	DATETIME NOT NULL,
	                `creation_user`	TEXT NOT NULL,
	                `change_date`	DATETIME NOT NULL,
	                `change_user`	TEXT NOT NULL,
	                `last_used_date`	DATETIME NOT NULL,
	                `last_used_user`	TEXT NOT NULL,
	                `usage_counter`	INT NOT NULL,
	                `flags`	INT
                );";
                command = new SQLiteCommand(sql, dbConnection);
                command.ExecuteNonQuery();
            }
            sql = @"CREATE TABLE IF NOT EXISTS `tsolution_terms` (
	            `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	            `source`	TEXT,
                `source_norm`	TEXT,
	            `target`	TEXT,
                `description`	TEXT	            
            );";
            command = new SQLiteCommand(sql, dbConnection);
            command.ExecuteNonQuery();

            sql = @"CREATE INDEX IF NOT EXISTS `tsolution_terms_index` on `tsolution_terms` (source_norm);";
            command = new SQLiteCommand(sql, dbConnection);
            command.ExecuteNonQuery();
        }

        public bool addToInternalTM(String source, String target)
        {
            if (exactSearchFST(source) != "") return false;
            string sql = @"INSERT INTO translation_units (
	            guid, translation_memory_id, source_hash, source_segment, target_hash, target_segment,
	            creation_date,  creation_user,
	            change_date, change_user,
	            last_used_date, last_used_user,
	            usage_counter,  flags)
                  values (@guid, @tmid, @shash, @source, @thash, @target, @crdate, @cruser, @chdate, @chuser, @ludate, @luuser, @ucounter, @flags)";
            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);            
            command.Parameters.Add(new SQLiteParameter("@guid", Guid.NewGuid()));
            command.Parameters.Add(new SQLiteParameter("@tmid", 1));
            command.Parameters.Add(new SQLiteParameter("@shash", source.GetHashCode()));
            command.Parameters.Add(new SQLiteParameter("@source", makeXmlValue(source)));
            command.Parameters.Add(new SQLiteParameter("@thash", target.GetHashCode()));
            command.Parameters.Add(new SQLiteParameter("@target", makeXmlValue(target)));
            command.Parameters.Add(new SQLiteParameter("@crdate", DateTime.Now));
            command.Parameters.Add(new SQLiteParameter("@cruser", "Rhea"));
            command.Parameters.Add(new SQLiteParameter("@chdate", DateTime.Now));
            command.Parameters.Add(new SQLiteParameter("@chuser", "Rhea"));
            command.Parameters.Add(new SQLiteParameter("@ludate", DateTime.Now));
            command.Parameters.Add(new SQLiteParameter("@luuser", "Rhea"));
            command.Parameters.Add(new SQLiteParameter("@ucounter", 1));
            command.Parameters.Add(new SQLiteParameter("@flags", 769));
            command.ExecuteNonQuery();

            command.CommandText = "INSERT INTO TSolution_search(source, target) Values(@src, @dest)";
            command.Parameters.Add(new SQLiteParameter("@src", source));
            command.Parameters.Add(new SQLiteParameter("@dest", target));
            command.ExecuteNonQuery();
            
            return true;
        }        

        public void setTMFiles(List<string> files)
        {
            tmFiles.Clear();
            tmFiles.AddRange(files);
            goodFTSIndex = false;
        }

        string getXmlValue(XmlDocument xmlDoc, string xml)
        {
            const string xpath = "Segment/Elements/Text/Value";
            xmlDoc.LoadXml(xml);
            var text = "";
            var nodes = xmlDoc.SelectNodes(xpath);
            foreach (XmlNode childrenNode in nodes)
            {
                text = text + childrenNode.InnerText;
            }
            return text;
        }
        string makeXmlValue(string xml)
        {
            return new XDocument(
                new XElement("Segment",
                new XElement("Elements",
                new XElement("Text",
                new XElement("Value", xml)
               )))).ToString();    
        }

        void AddTMToFST(SQLiteConnection tmConnection)
        {
            String sql = "select * from translation_units";
            var command = new SQLiteCommand(sql, tmConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            XmlDocument xmlDoc = new XmlDocument();

            SQLiteCommand sqCommand = new SQLiteCommand();
            sqCommand.Connection = dbConnection;
            SQLiteTransaction myTrans;

            // Start a local transaction 
            myTrans = dbConnection.BeginTransaction();
            // Assign transaction object for a pending local transaction 
            sqCommand.Transaction = myTrans;

            try
            {
                sqCommand.CommandText = "INSERT INTO TSolution_search(source, target) Values(@src, @dest)";
                var par1 = new SQLiteParameter("@src");
                sqCommand.Parameters.Add(par1);
                var par2 = new SQLiteParameter("@dest");
                sqCommand.Parameters.Add(par2);

                while (reader.Read())
                {
                    var source = getXmlValue(xmlDoc, reader["source_segment"].ToString());
                    var dest = getXmlValue(xmlDoc, reader["target_segment"].ToString());
                    if (source.Length > 1)
                    {
                        par1.Value = Regex.Replace(source, @"[^\w\s]", "");
                        par2.Value = Regex.Replace(dest, @"[^\w\s]", "");
                        sqCommand.ExecuteNonQuery();
                    }
                }

                myTrans.Commit();

            }
            catch (Exception e)
            {
                myTrans.Rollback();
                Console.WriteLine(e.ToString());
                Console.WriteLine("Neither record was written to database.");
            }
        }
        void prepareFullTextsearchTable()
        {
            if (goodFTSIndex)
                return;
            goodFTSIndex = true;
            string sql = "DROP TABLE IF EXISTS TSolution_search";
            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
            command.ExecuteNonQuery();
            
            sql = "CREATE VIRTUAL TABLE TSolution_search USING fts4 (id, source, target);";
            command = new SQLiteCommand(sql, dbConnection);
            command.ExecuteNonQuery();

            AddTMToFST(dbConnection); // adding internal TM
            foreach (var tmFileName in tmFiles)
            {
                var tmConnection = new SQLiteConnection(string.Format("Data Source={0};Version=3;New=False;Compress=True;", tmFileName));
                tmConnection.Open();
                AddTMToFST(tmConnection);
                tmConnection.Close();
            }
        }

        public void updateTM()
        {
            goodFTSIndex = false;
            prepareFullTextsearchTable();
        }

        public List<TMUnit> searchFST(string phrase)
        {
            prepareFullTextsearchTable();
            var result = new List<TMUnit>();

            string sql = "SELECT * FROM TSolution_search WHERE source MATCH @search";
            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
            var par1 = new SQLiteParameter("@search");
            command.Parameters.Add(par1);
            par1.Value = Regex.Replace(phrase, @"[^\w\s]", "");
            SQLiteDataReader reader = command.ExecuteReader();
            
            while (reader.Read())
            {
                var source = reader["source"].ToString();
                var dest = reader["target"].ToString();;
                result.Add(new TMUnit(source, dest));
            }

            return result;
        }

        string normString(string phrase)
        {
            char[] endParts = { ' ', '!', '?', ':', '\n', '\t', '\r', '.', ',', '(', ')' , '-'};
            var words = phrase.Split(endParts, StringSplitOptions.RemoveEmptyEntries);            
            return String.Join("", words);
        }

        public string exactSearchFST(string phrase)
        {
            char[] endParts = { ' ', '!', '?', ':', '\n', '\t', '\r', '.', ',' };
            var trimPhrase = phrase.TrimEnd(endParts);
            var PhraseEnding = phrase.Remove(0, trimPhrase.Length);
            var normPhrase = normString(phrase);
            prepareFullTextsearchTable();
            
            string sql = "SELECT * FROM TSolution_search WHERE source MATCH @search";
            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
            var par1 = new SQLiteParameter("@search");
            command.Parameters.Add(par1);
            par1.Value = Regex.Replace(phrase, @"[^\w\s]", "");
            SQLiteDataReader reader = command.ExecuteReader();

            var result = "";
            while (reader.Read())
            {
                if (normString(reader["source"].ToString()) == normPhrase)
                    result = reader["target"].ToString().TrimEnd(endParts) + PhraseEnding;
            }

            return result;
        }  

        public string LemmatizeSourcePhrase(string phrase)
        {
            return WordList.LemmatizePhrase(phrase, sourceLemmatizer);
        }

        public TermsCollection searchTerms(string phrase)
        {
            var results = new TermsCollection();
            var words = new WordList(phrase, sourceLemmatizer);
            var normPhrase = WordList.LemmatizePhrase(phrase, sourceLemmatizer);

            string sql = "SELECT * FROM tsolution_terms WHERE source_norm = @search";
            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
            var par1 = new SQLiteParameter("@search");
            command.Parameters.Add(par1);

            const int maxWords = 4;

            for(var i = 0; i < words.Count; i++)
            {
                var word = words[i];
                var curPhrase = word.Lemma;
                var phraseStart = word.Position;
                var phraseLength = word.Word.Length;
                for (var j = 1; j <= maxWords; j++)
                {
                    par1.Value = curPhrase;
                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var Term = reader["source"].ToString();
                        var Target = reader["target"].ToString();
                        var Description = reader["description"].ToString();
                        var id = (long)reader["id"];
                        var term = results.AddTerm(Term, curPhrase);
                        term.addTranslation(Target, Description, id);
                        term.addEntry(phraseStart, phraseLength);
                    }
                    reader.Close();

                    if (i + j < words.Count)
                    {
                        word = words[i + j];
                        curPhrase += ' ' + word.Lemma;
                        phraseLength = word.Position - phraseStart + word.Word.Length;
                    }
                    else
                        break;
                }
            }
            return results;
        }

        public bool addTerm(string Term, string Translation, string Description)
        {
            var normTerm = WordList.LemmatizePhrase(Term, sourceLemmatizer);
            var sql = "INSERT INTO TSolution_terms(source, source_norm, target, description) Values(@src, @srcnorm, @dest, @desc)";
            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);            
            command.Parameters.Add(new SQLiteParameter("@src", Term));
            command.Parameters.Add(new SQLiteParameter("@srcnorm", normTerm));
            command.Parameters.Add(new SQLiteParameter("@dest", Translation));
            command.Parameters.Add(new SQLiteParameter("@desc", Description));
            command.ExecuteNonQuery();            
            return true;
        }

        public bool updateTerm(TermVariant term)
        {
            var normTerm = WordList.LemmatizePhrase(term.Source, sourceLemmatizer);
            var sql = @"UPDATE TSolution_terms
                SET source = @src, source_norm = @srcnorm, target = @dest, description = @desc
                WHERE id = @id";
            SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
            command.Parameters.Add(new SQLiteParameter("@id", term.UID));
            command.Parameters.Add(new SQLiteParameter("@src", term.Source));
            command.Parameters.Add(new SQLiteParameter("@srcnorm", normTerm));
            command.Parameters.Add(new SQLiteParameter("@dest", term.Value));
            command.Parameters.Add(new SQLiteParameter("@desc", term.Description));
            command.ExecuteNonQuery();
            return true;
        }
    }
}