﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;
using System.IO;

namespace OfficeTranslator
{
    public class TSSettings    
    {
        List<string> tmfiles = new List<string>();
        public List<string> TMFiles { get { return tmfiles; }  }
        public bool MultitranOnDblClick = true;
        public void Save(string FileName)
        {
            using (var writer = new System.IO.StreamWriter(FileName))
            {
                var serializer = new XmlSerializer(this.GetType());
                serializer.Serialize(writer, this);
                writer.Flush();
            }
        }

        public static TSSettings Load(string FileName)
        {
            try
            {
                using (var stream = System.IO.File.OpenRead(FileName))
                {
                    var serializer = new XmlSerializer(typeof(TSSettings));
                    return serializer.Deserialize(stream) as TSSettings;
                }
            }
            catch
            {
                return new TSSettings();
            }
        }
    }
    public class TSProject
    {
        TSSettings settings;
        private TranslationMemoryDatabase translationMemory;
        private TranslateControl transControl;
        private MultitranControl multitranControl;
        private Microsoft.Office.Tools.CustomTaskPane transPane;
        private Microsoft.Office.Tools.CustomTaskPane multitranPane;
        private string configFile;
        private string dbFile;
        public TSProject(Word.Window window)
        {
            Window = window;
            configFile = Path.ChangeExtension(window.Document.FullName, ".tsconfig");
            dbFile = Path.ChangeExtension(window.Document.FullName, ".tsdata");
            settings = TSSettings.Load(configFile);
            translationMemory = new TranslationMemoryDatabase(dbFile, settings.TMFiles);            
            transControl = new TranslateControl(translationMemory);
            transControl.MultitranOnDblClick = settings.MultitranOnDblClick;
            transControl.OnMultitranSearch = term =>
                {
                    MultitranPane.Visible = true;
                    multitranControl.DisplayTerm(term);
                };
            transPane = Globals.ThisAddIn.CustomTaskPanes.Add(transControl, "Translation", window);            
            transPane.DockPosition = Microsoft.Office.Core.MsoCTPDockPosition.msoCTPDockPositionBottom;
            transPane.Visible = true;            
        }

        public Microsoft.Office.Tools.CustomTaskPane MultitranPane 
        { 
            get { 
                if (multitranPane == null)
                {
                    multitranControl = new MultitranControl();
                    multitranPane = Globals.ThisAddIn.CustomTaskPanes.Add(multitranControl, "Multitran", Window);
                    multitranPane.DockPosition = Microsoft.Office.Core.MsoCTPDockPosition.msoCTPDockPositionLeft;
                    multitranPane.Width = 400;
                    multitranPane.Visible = true;
                }
                return multitranPane;  
            } 
        }

        public bool MultitranPaneVisible
        {
            get { return multitranPane != null ? multitranPane.Visible: false; }
            set { MultitranPane.Visible = value; }
        }

        public void UpdateTM()
        {
            translationMemory.updateTM();
        }

        public void Flush()
        {
            translationMemory.setTMFiles(settings.TMFiles);
            settings.Save(configFile);
        }

        public TranslateControl TranslControl { get { return transControl; }}
        public Word.Window Window { get; set; }
        public Microsoft.Office.Tools.CustomTaskPane Pane { get { return transPane; } }
        public TSSettings Settings { get { return settings; } }
    }
}
