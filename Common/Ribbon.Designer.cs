﻿namespace OfficeTranslator
{
    partial class Ribbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public Ribbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabSolution = this.Factory.CreateRibbonTab();
            this.groupJump = this.Factory.CreateRibbonGroup();
            this.btnJumpSentence = this.Factory.CreateRibbonButton();
            this.btnJumpParagraph = this.Factory.CreateRibbonButton();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.btnTranslationPane = this.Factory.CreateRibbonToggleButton();
            this.btnMultitranPane = this.Factory.CreateRibbonToggleButton();
            this.checkBoxMultitran = this.Factory.CreateRibbonCheckBox();
            this.groupTM = this.Factory.CreateRibbonGroup();
            this.btnTM = this.Factory.CreateRibbonButton();
            this.btnUpdateTM = this.Factory.CreateRibbonButton();
            this.openTMDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnStart = this.Factory.CreateRibbonButton();
            this.tabSolution.SuspendLayout();
            this.groupJump.SuspendLayout();
            this.group1.SuspendLayout();
            this.groupTM.SuspendLayout();
            // 
            // tabSolution
            // 
            this.tabSolution.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tabSolution.Groups.Add(this.groupJump);
            this.tabSolution.Groups.Add(this.group1);
            this.tabSolution.Groups.Add(this.groupTM);
            this.tabSolution.Label = "OfficeTranslator";
            this.tabSolution.Name = "tabSolution";
            // 
            // groupJump
            // 
            this.groupJump.Items.Add(this.btnStart);
            this.groupJump.Items.Add(this.btnJumpSentence);
            this.groupJump.Items.Add(this.btnJumpParagraph);
            this.groupJump.Label = "Translation";
            this.groupJump.Name = "groupJump";
            // 
            // btnJumpSentence
            // 
            this.btnJumpSentence.Label = "Next sentence";
            this.btnJumpSentence.Name = "btnJumpSentence";
            this.btnJumpSentence.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnJumpSentence_Click);
            // 
            // btnJumpParagraph
            // 
            this.btnJumpParagraph.Label = "Next section";
            this.btnJumpParagraph.Name = "btnJumpParagraph";
            this.btnJumpParagraph.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnJumpParagraph_Click);
            // 
            // group1
            // 
            this.group1.Items.Add(this.btnTranslationPane);
            this.group1.Items.Add(this.btnMultitranPane);
            this.group1.Items.Add(this.checkBoxMultitran);
            this.group1.Label = "Settings";
            this.group1.Name = "group1";
            // 
            // btnTranslationPane
            // 
            this.btnTranslationPane.Checked = true;
            this.btnTranslationPane.Label = "Translation pane";
            this.btnTranslationPane.Name = "btnTranslationPane";
            this.btnTranslationPane.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.toggleButton1_Click);
            // 
            // btnMultitranPane
            // 
            this.btnMultitranPane.Label = "Multitran pane";
            this.btnMultitranPane.Name = "btnMultitranPane";
            this.btnMultitranPane.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnMultitranPane_Click);
            // 
            // checkBoxMultitran
            // 
            this.checkBoxMultitran.Checked = true;
            this.checkBoxMultitran.Label = "Multitran on double click";
            this.checkBoxMultitran.Name = "checkBoxMultitran";
            this.checkBoxMultitran.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.checkBoxMultitran_Click);
            // 
            // groupTM
            // 
            this.groupTM.Items.Add(this.btnTM);
            this.groupTM.Items.Add(this.btnUpdateTM);
            this.groupTM.Label = "Translation memory";
            this.groupTM.Name = "groupTM";
            // 
            // btnTM
            // 
            this.btnTM.Label = "Add TM";
            this.btnTM.Name = "btnTM";
            this.btnTM.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnTM_Click);
            // 
            // btnUpdateTM
            // 
            this.btnUpdateTM.Label = "Update TM";
            this.btnUpdateTM.Name = "btnUpdateTM";
            this.btnUpdateTM.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnUpdateTM_Click);
            // 
            // openTMDialog
            // 
            this.openTMDialog.Filter = "TM |*.sdltm|All files (*.*)|*.*";
            // 
            // btnStart
            // 
            this.btnStart.Label = "Start translation!";
            this.btnStart.Name = "btnStart";
            this.btnStart.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnStart_Click);
            // 
            // Ribbon
            // 
            this.Name = "Ribbon";
            this.RibbonType = "Microsoft.Word.Document";
            this.Tabs.Add(this.tabSolution);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon1_Load);
            this.tabSolution.ResumeLayout(false);
            this.tabSolution.PerformLayout();
            this.groupJump.ResumeLayout(false);
            this.groupJump.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.groupTM.ResumeLayout(false);
            this.groupTM.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tabSolution;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup groupJump;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnJumpSentence;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnJumpParagraph;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton btnTranslationPane;
        private System.Windows.Forms.OpenFileDialog openTMDialog;
        internal Microsoft.Office.Tools.Ribbon.RibbonCheckBox checkBoxMultitran;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup groupTM;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnTM;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnUpdateTM;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton btnMultitranPane;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnStart;
    }

    partial class ThisRibbonCollection
    {
        internal Ribbon Ribbon1
        {
            get { return this.GetRibbon<Ribbon>(); }
        }
    }
}
